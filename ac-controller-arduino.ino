#include <DYIRDaikin.h>

#include "protocol.h"

//#define DYIRDAIKIN_SOFT_IR

DYIRDaikin irdaikin;
int isOn;

void setup()
{
  Serial.begin(9600);

#ifdef DYIRDAIKIN_SOFT_IR
  irdaikin.begin(3);
#else
  irdaikin.begin();
#endif
  irdaikin.on();
  irdaikin.setSwing_off();
  irdaikin.setMode(1);
  irdaikin.setFan(0);
  irdaikin.setTemp(27);
  irdaikin.sendCommand();
  isOn = 0;
}

void loop() {
  while (Serial.available() > 0) {
    switch (Serial.read()) {
    case INFO:
      Serial.print("{");
      Serial.print("\"power\":");
      if (irdaikin.getPower() > 0) {
        Serial.print("true");
      } else {
        Serial.print("false");
      }
      Serial.print(",");
      Serial.print("\"temperature\":");
      Serial.print(irdaikin.getTemp(), DEC);
      Serial.println("}");
      break;
    case POWER_ON:
      irdaikin.setPower(1);
      break;
    case POWER_OFF:
      irdaikin.setPower(0);
      break;
    case TOO_HOT:
      irdaikin.setTemp(irdaikin.getTemp() - 1);
      break;
    case TOO_COLD:
      irdaikin.setTemp(irdaikin.getTemp() + 1);
      break;
    case '\r':
      // Power
      Serial.print("Power: ");
      Serial.println(irdaikin.getPower());

      // Temperature
      Serial.print("Temperature: ");
      Serial.println(irdaikin.getTemp());
      break;
    case '\n':
      if (isOn == 0) {
        isOn = 1;
        irdaikin.off();
        Serial.println("Turn Off");
      } else {
        isOn = 0;
        irdaikin.on();
        Serial.println("Turn On");
      }
      break;
    }
    irdaikin.sendCommand();
  }

  delay(10);
}
