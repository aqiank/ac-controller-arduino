#pragma once

enum Protocol {
  INFO = 0xA0,
  POWER_OFF = 0xA1,
  POWER_ON = 0xA2,
  TOO_HOT = 0xA3,
  TOO_COLD = 0xA4,
};

